export const urgencies = {
    'High': 'Невідкладна',
    'Normal': 'Пріоритетна',
    'Low': 'Звичайна'
}